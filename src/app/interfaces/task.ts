export interface Task {
    text: string;
    id: number;
    complited: boolean;
  }