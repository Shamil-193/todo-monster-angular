import { Injectable } from "@angular/core";
import { BehaviorSubject } from "rxjs";
import { Task } from "../interfaces/task";


@Injectable({
  providedIn: "root"
})
export class TasksService {

  tasks: Task[]=[]
  tasks$=new BehaviorSubject<Task[]>(this.tasks)

  setLocalStorage(tasks: Task[]) {
    this.tasks$.next(tasks)
    localStorage.setItem("tasks", JSON.stringify(tasks))
  }

  getLocalStorage() {
    return JSON.parse(localStorage.getItem("tasks")!)
  }

  create(taskText: string) {
    if (taskText.trim()) {
      const task: Task={
        text: taskText,
        id: this.getId(),
        complited: false
      }
      let TasksFromMemory: Task[]=this.getLocalStorage()
      TasksFromMemory.push(task)
      this.setLocalStorage(TasksFromMemory)
    }
  }

  update(inputTask: Task) {
    let TasksFromMemory: Task[]=this.getLocalStorage()
    TasksFromMemory=TasksFromMemory.map(task => task.id === inputTask.id ? inputTask : task)
    this.setLocalStorage(TasksFromMemory)
  }

  delete(id: number) {
    let TasksFromMemory: Task[]=this.getLocalStorage()
    TasksFromMemory=TasksFromMemory.filter(task => task.id !== id)
    this.setLocalStorage(TasksFromMemory)
  }

  getId() {
    const idCounter=JSON.parse(localStorage.getItem("ID")!)
    if (idCounter === null) {
      localStorage.setItem("ID", JSON.stringify(1))
      return 0
    } else {
      localStorage.setItem("ID", JSON.stringify(idCounter + 1))
      return idCounter
    }
  }

  find(): void {
    this.tasks$.next(this.getLocalStorage())
  }

}
