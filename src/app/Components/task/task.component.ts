import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from "@angular/core";
import { Task } from "src/app/interfaces/task.js";

@Component({
  selector: "app-task",
  templateUrl: "./task.component.html",
  styleUrls: ["./task.component.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush
})

export class TaskComponent {

  @Input() task: Task
  @Output() onTaskComplite=new EventEmitter<Task>()
  @Output() onTaskDelete=new EventEmitter<number>()
  @Output() onTaskTouch=new EventEmitter<Task>()

  taskComplite() {
    this.onTaskComplite.emit(this.task)
  }

  taskDelete() {
    this.onTaskDelete.emit(this.task.id)
  }

  taskTouch() {
    this.onTaskTouch.emit(this.task)
  }
}
