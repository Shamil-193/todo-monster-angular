import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from "@angular/core";
import { Task } from "src/app/interfaces/task.js";


@Component({
  selector: "app-task-form",
  templateUrl: "./task-form.component.html",
  styleUrls: ["./task-form.component.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush
})

export class TaskFormComponent {

  @Input() touchedTask: Task | null
  @Output() onTaskCreate=new EventEmitter<string>()
  @Output() onTaskUpdate=new EventEmitter<Task>()

  createTask(text: string) {
    this.onTaskCreate.emit(text)
  }

  updateTask(text: string) {
    if (this.touchedTask) {
      const updatedTask: Task={
        id: this.touchedTask.id,
        text: text,
        complited: this.touchedTask.complited
      }
      this.onTaskUpdate.emit(updatedTask)
      this.cancelTask()
    }
  }

  cancelTask() {
    this.touchedTask=null
  }
}
