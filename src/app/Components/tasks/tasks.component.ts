import { ChangeDetectionStrategy, Component } from '@angular/core';
import { Observable } from 'rxjs';
import { Task } from "src/app/interfaces/task.js";
import { TasksService } from 'src/app/services/tasks.service';

@Component({
  selector: 'app-tasks',
  templateUrl: './tasks.component.html',
  styleUrls: ['./tasks.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TasksComponent {

  tasks$: Observable<Task[]>=this.tasksService.tasks$
  touchedTask: Task

  constructor(private tasksService: TasksService) {
    this.tasksService.find()
  }

  taskCreate(text: any) {
    this.tasksService.create(text)
  }

  taskDelete(id: number) {
    this.tasksService.delete(id)
  }

  taskComplite(task: Task) {
    task.complited=!task.complited
    this.tasksService.update(task)
  }

  taskTouch(task: Task) {
    this.touchedTask=task
  }

  taskUpdate(task: Task) {
    this.tasksService.update(task)
  }
}
